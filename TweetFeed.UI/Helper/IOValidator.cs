﻿using System;
using System.IO;
using TweetFeed.Core.Helper;

namespace TweetFeed.UI.Helper
{
    /// <summary>
    /// Static class to help with IO validation.
    /// </summary>
    public static class IOValidator
    {
        #region Public Methods

        /// <summary>
        /// Validates a local file path.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool ValidatePath(string path)
        {
            try
            {
                var info = new FileInfo(path);
                return info.Exists;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Validate a file extension against the allowed input file extensions.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool ValidateFileType(string path)
        {
            if (!ValidatePath(path))
                return false;

            var ext = Path.GetExtension(path);
            return string.Equals(ext, Constants.InputFileExtension);
        }

        #endregion
    }
}
