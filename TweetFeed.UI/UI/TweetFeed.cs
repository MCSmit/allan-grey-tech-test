﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TweetFeed.Core.Helper;
using TweetFeed.Data.Entity;
using TweetFeed.Data.Helper;
using TweetFeed.Data.Repository;
using TweetFeed.UI.Helper;

namespace TweetFeed.UI.UI
{
    /// <summary>
    /// Entry to main console application.
    /// Application mimics a Twitter feed by loading a User and Tweet file and generating output in the format of a Twitter feed.
    /// </summary>
    public class TweetFeed
    {
        #region Application Entry

        /// <summary>
        /// Entry point to TweetFeed console application.
        /// </summary>
        /// <param name="args[0]">String with path to User data file.</param>
        /// <param name="args[1]">String with path to Tweet data file.</param>
        public static void Main(string[] args)
        {
            StartTweetFeed(args);
            Console.ReadLine();
        }
        
        #endregion

        #region Private Methods

        /// <summary>
        /// Static method to call the Async initializer method, and perform some basic input validation.
        /// </summary>
        /// <param name="args"></param>
        private static void StartTweetFeed(string[] args)
        {
            //Validate Input
            if (args == null || args.Count() != 2)
            {
                Console.WriteLine("Please provide the [User] and [Data] file paths as input arguments [1] and [2].");
                Console.ReadLine();
                return;
            }

            var usersFile = args[0];
            var tweetFile = args[1];

            if (!IOValidator.ValidatePath(usersFile))
            {
                var userFileName = Path.GetFileName(usersFile);
                Console.WriteLine("The file path for {0} cannot be confirmed. Please specify another path.",
                    string.IsNullOrEmpty(userFileName) ? "[User].txt" : userFileName);
                return;
            }
            if (!IOValidator.ValidateFileType(usersFile))
            {
                Console.WriteLine("Please ensure the {0} file has a {1} extension.",
                    "[User]",
                    Constants.InputFileExtension);
                return;
            }
            if (!IOValidator.ValidatePath(tweetFile))
            {
                var tweetFileName = Path.GetFileName(tweetFile);
                Console.WriteLine("The file path for {0} cannot be confirmed. Please specify another path.", 
                    string.IsNullOrEmpty(tweetFileName) ? "[Data].txt" : tweetFileName);
                return;
            }
            if (!IOValidator.ValidateFileType(tweetFile))
            {
                Console.WriteLine("Please ensure the {0} file has a {1} extension.",
                    "[Data]",
                    Constants.InputFileExtension);
                return;
            }

            string result = string.Empty;
            do
            {
                Console.Write("Do you want to simulate a long-running process (Y/N):");
                result = Console.ReadLine();

            } while (!new[] { "y", "n", }.Contains(result.ToLower()));

            Task.Run(async () => await (Task.WhenAll(StartTweetFeedAsync(args[0], args[1], result.ToLower() == "y" ? true : false))));
        }

        /// <summary>
        /// Initialize the mock Twitter feed process.
        /// </summary>
        /// <param name="userFile">String with path to User data file.</param>
        /// <param name="tweetFile">String with path to Tweet data file.</param>
        /// <param name="simulate">Bool indicating to run long-running simulation.</param>
        private static async Task StartTweetFeedAsync(string userFile, string tweetFile, bool simulate)
        {
            // Declare var's outside Try/Catch to allow access in the Catch event.
            Task loadResult = null;
            var progressCTS = new CancellationTokenSource();

            try
            {
                //Start updating the UI with progress info.
                Console.Clear();
                Console.Write("Loading User and Data files... ");
                UpdateUIAsync(progressCTS.Token);

                //Start load operations.
                var repository = new Repository();
                var userLoadTask = repository.LoadAsync<User>(userFile);
                var tweetLoadTask = repository.LoadAsync<Tweet>(tweetFile);

                //Wait for both files to be loaded before continuing.
                await (loadResult = Task.WhenAll(userLoadTask, tweetLoadTask));

                //Once load operations have completed, cancel the UI progress indicator process.
                if (simulate)
                    Thread.Sleep(10000);
                progressCTS.Cancel();

                Console.Clear();
                Console.WriteLine("Load operations completed.");
                Thread.Sleep(3000);

                //Print Twitter mock feed.
                PrintTweetFeed(userLoadTask.Result, tweetLoadTask.Result);
            }
            catch (Exception ex)
            {
                progressCTS.Cancel();
                Console.WriteLine("Unexpected Error: {0}", ex.Message);
                if (loadResult != null)
                    foreach (var exception in loadResult.Exception.InnerExceptions)
                        Console.WriteLine("Error Details: {0}", exception.Message);
            }
        }

        /// <summary>
        /// Outputs a mock Twitter feed to the Console.
        /// </summary>
        /// <param name="userData">List of User objects.</param>
        /// <param name="tweetData">List of Tweet objects.</param>
        private static void PrintTweetFeed(List<User> userData, List<Tweet> tweetData)
        {
            try
            {
                Console.Clear();
                var data = TweetFeedGenerator.GenerateTweetFeed(userData, tweetData);

                if (data.Count == 0)
                    Console.WriteLine("No Tweets to display.");
                else
                    foreach (var o in data.OrderBy(o => o.Key.UserName))
                    {
                        Console.WriteLine(o.Key.UserName);

                        foreach (var tweet in o.Value.OrderBy(t => t.TimeStamp))
                            Console.WriteLine("\t@{0}: {1}", tweet.User.UserName, tweet.Message);

                        Console.WriteLine("\n");
                    }

                Console.Write("Press any key to continue...");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unexpected Error: {0}", ex.Message);
            }
        }

        /// <summary>
        /// UI function called Async to output a repetitive progress indicator loop. Infinite function,
        /// canceled via the CancellationToken.
        /// </summary>
        /// <param name="progressCT"></param>
        private static async void UpdateUIAsync(CancellationToken progressCT)
        {
            await Task.Run(() =>
            {
                using (var progress = new ProgressBar())
                {
                    var i = 0;
                    while (!progressCT.IsCancellationRequested)
                    {
                        progress.Report((double) i++/100);
                        i = i <= 100 ? i : 0;
                        Thread.Sleep(20);
                    }
                }
            },
                progressCT);
        }

        #endregion
    }
}
