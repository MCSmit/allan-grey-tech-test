﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TweetFeed.Data.Helper;

namespace TweetFeed.Data.Repository
{
    public class Repository
    {
        #region Private Fields

        FileLoader _FileLoader = new FileLoader();

        #endregion  

        #region Public Methods

        /// <summary>
        /// This thod has been left Public for instances where Async loading is not required.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public List<T> Load<T>(string filePath)
        {
            return this._FileLoader.LoadFile<T>(filePath);
        }

        public Task<List<T>> LoadAsync<T>(string filePath)
        {
            return Task.Run<List<T>>(() => Load<T>(filePath));
        }

        #endregion
    }
}
