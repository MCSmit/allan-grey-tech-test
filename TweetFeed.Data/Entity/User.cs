﻿using System.Collections.Generic;

namespace TweetFeed.Data.Entity
{
    /// <summary>
    /// This object represents a Twitter User. 
    /// Due to the nature of this exercise, UserName acts as the unique identifier for this object.
    /// </summary>
    public class User
    {
        #region Constructor(s)

        public User()
        {
            this.Associations = new List<User>();
        }

        #endregion

        #region Public Properties

        public string UserName { get; set; }
        public List<User> Associations { get; set; }

        #endregion
    }
}
