﻿using System;

namespace TweetFeed.Data.Entity
{
    /// <summary>
    /// This object represents a Twitter Tweet. 
    /// Due to the nature of this exercise, this object does not have a unique identifier.
    /// </summary>
    public class Tweet
    {
        #region Public Properties

        public User User { get; set; }
        public string Message { get; set; }
        public DateTime TimeStamp { get; set; }

        #endregion
    }
}
