﻿using System.Collections.Generic;
using System.Linq;
using TweetFeed.Data.Entity;

namespace TweetFeed.Data.Helper
{
    /// <summary>
    /// Static class that matches users to their and their associations' Tweets. 
    /// </summary>
    public static class TweetFeedGenerator
    {
        /// <summary>
        /// Match users to their and their associations' Tweets and returns a Dictionary collection.
        /// </summary>
        /// <param name="users"></param>
        /// <param name="tweets"></param>
        /// <returns></returns>
        public static Dictionary<User, List<Tweet>> GenerateTweetFeed(List<User> users, List<Tweet> tweets)
        {
            var data = new Dictionary<User, List<Tweet>>();

            users.ForEach(user =>
            {
                data.Add(user,
                    tweets.Where(t =>
                    t.User.UserName == user.UserName ||
                    user.Associations.Select(a => a.UserName).ToArray().Contains(t.User.UserName)).ToList());
            });

            return data;
        } 
    }
}
