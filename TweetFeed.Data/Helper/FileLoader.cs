﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TweetFeed.Core.Helper;
using TweetFeed.Data.Entity;
using TweetFeed.Data.Helper.Interface;

namespace TweetFeed.Data.Helper
{
    /// <summary>
    /// Helper class for Loading and Extracting <T> data from a file.
    /// </summary>
    public class FileLoader: IFileLoader
    {
        #region Private Methods

        /// <summary>
        /// Extract Tweet data from User file array.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private List<Tweet> ExtractTweets(string[] data)
        {
            // Assumption: Since there is no other way to validate a well-formed Tweet line, we're checking that each line contains the delimiter '>'.
            return (from o in data.Where(t => t.Contains(Constants.TweetDelimiter))
                select o.Split(new[] { Constants.TweetDelimiter }, StringSplitOptions.RemoveEmptyEntries)
                into rawData
                where rawData.Length == 2
                select new Tweet
                {
                    User = new User {UserName = rawData[0].Trim()},
                    Message = rawData[1].Trim().Substring(0, rawData[1].Trim().Length <= Constants.MaxLength ? rawData[1].Trim().Length : Constants.MaxLength),
                    TimeStamp = DateTime.Now
                }).ToList();
        }

        /// <summary>
        /// Extract User data from User file array.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private List<User> ExtractUsers(string[] data)
        {
            var users = new List<User>();

            // Assumption: Since there is no other way to validate a well-formed User line, we're checking that each line contains the delimiter 'follows'.
            foreach (var o in data.Where(u => u.Contains(Constants.UserDelimiter)))
            {
                var rawData = o.Split(new[] { Constants.UserDelimiter }, StringSplitOptions.RemoveEmptyEntries);
                var user = new User();

                if (rawData.Any())
                    user.UserName = rawData[0].Trim();

                if (rawData.Length > 1)
                {
                    foreach (var f in rawData[1].Split(new[] { Constants.AssociationDelimiter }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        user.Associations.Add(new User { UserName = f.Trim() });
                        if (!users.Any(u => u.UserName == f.Trim()))
                            users.Add(new User { UserName = f.Trim() });
                    }
                }

                // Update Existing User
                if (users.Any(u => u.UserName == user.UserName))
                {
                    var existingUser = users.FirstOrDefault(u => u.UserName == user.UserName);
                    user.Associations.ForEach(i =>
                    {
                        if (!existingUser.Associations.Any(f => f.UserName == i.UserName))
                            existingUser.Associations.Add(i);
                    });
                    continue;
                }

                // Add new User
                users.Add(user);
            }

            return users;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Load data from file into a string array and pass array to Extraction method to return List<T>.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public List<T> LoadFile<T>(string filePath)
        {
            try
            {
                var rawData = File.ReadAllLines(filePath);

                if (rawData.Length == 0)
                    return new List<T>();

                if (typeof(T) == typeof(User))
                    return ExtractUsers(rawData) as List<T>;
                if (typeof(T) == typeof(Tweet))
                    return ExtractTweets(rawData) as List<T>;
                
                return new List<T>();
                    
            }
            catch (Exception)
            {
                return new List<T>();
            }
        }

        #endregion
    }
}
