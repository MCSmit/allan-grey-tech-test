﻿using System.Collections.Generic;

namespace TweetFeed.Data.Helper.Interface
{
    public interface IFileLoader
    {
        List<T> LoadFile<T>(string filePath);
    }
}
