/***********/
 TweetFeed
/***********/

Author: Marthinus C. (Riaan) Smit
Contact: 079 066 7252
Email: mc_smit27@hotmail.com
Date: 27/05/2016

Tools Used: Visual Studio 2015 (C#) 

Goal: The aim of the project is to load a specified User and Tweet data file and simulate a Twitter like feed.

Assumptions:
1) The load functions are performed async by the FileLoader.cs files in the Data project. At this point if any exceptions occur, an empty collection is returned instead of an error message. I considered writing the error feedback to a file, but concluded that the basics of error handling is already displayed.
2) The actual data lists used for generating the output is loaded by the FileLoader.cs files in the Data project. If the file contains data that can be forced into the specified format, then it will be done (basically dependent on the delimiter), otherwise the line will be seen as mall-formed and skipped.
3) In Debug, the two files specified are at C:\, and are User.txt and Tweet.txt (both have been included in the project folder should you want to use them.) 

As per the assignment:
1) The user file input format is expected to be: User 'follows' User,User,User, where the word 'follows' is the delimiter for the User from it's associations, and the associations are delimited by a comma.
2) The tweet file input format is expected to be: User '>' message, where the great-than sign is the delimiter for the User from the message with a max of 140 allowed characters.

Note:
With the idea that each input file might contain thousands/millions of lines, I decided to call both load functions async and show some primitive form of progress indication on in the UI.

TO DO's:
I would also have liked to explore other methods of dealing with the Async functions and cancellation there off.
I also think that the Extract function for Users can be done completely in Linq/Lambda, but might make reading/maintenance a bit more tricky then it's worth.

Thank you for the opportunity to be a part of this interview process.
Looking forward to hearing from you.

Kind regards,
Riaan Smit