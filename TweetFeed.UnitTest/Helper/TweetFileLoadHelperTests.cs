﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TweetFeed.Data.Helper;
using System.Linq;
using TweetFeed.Data.Entity;

namespace TweetFeed.UnitTest.Helper
{
    [TestClass()]
    public class TweetFileLoadHelperTests
    {
        [TestMethod()]
        public void LoadFileTest_NoInput()
        {
            var fileLoader = new FileLoader();
            var result = fileLoader.LoadFile<Tweet>(string.Empty);
            Assert.IsFalse(result.Any(), "Expecting an empty List<Tweet> instance.");
        }

        [TestMethod()]
        public void LoadFileTest_InvalidInput()
        {
            var fileLoader = new FileLoader();
            var result = fileLoader.LoadFile<Tweet>(@"Q:\Tweet.txt");
            Assert.IsFalse(result.Any(), "Expecting an empty List<Tweet> instance.");
        }

        [TestMethod()]
        public void LoadFileTest_ValidInput()
        {
            var fileLoader = new FileLoader();
            var result = fileLoader.LoadFile<Tweet>(@"C:\Tweet.txt");
            Assert.AreEqual(3, result.Count, "Expecting a List<Tweet> instance with 3 items.");
        }
    }
}