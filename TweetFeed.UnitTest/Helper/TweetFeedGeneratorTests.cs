﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TweetFeed.Data.Helper;
using System.Collections.Generic;
using System.Linq;
using TweetFeed.Data.Entity;

namespace TweetFeed.UnitTest.Helper
{
    [TestClass()]
    public class TweetFeedGeneratorTests
    {
        [TestMethod()]
        public void GenerateTweetFeedTest_EmptyUserInput()
        {
            var fileLoader = new FileLoader();
            var userData = new List<User>();
            var tweetData = fileLoader.LoadFile<Tweet>(@"C:\Tweet.txt");
            var data = TweetFeedGenerator.GenerateTweetFeed(userData, tweetData);
            Assert.AreEqual<int>(0, data.Count, "Expecting a Dictionary<User,List<Tweet>> instance with 0 items.");
        }

        [TestMethod()]
        public void GenerateTweetFeedTest_EmptyTweetInput()
        {
            var fileLoader = new FileLoader();
            var userData = fileLoader.LoadFile<User>(@"C:\User.txt");
            var tweetData = new List<Tweet>();
            var data = TweetFeedGenerator.GenerateTweetFeed(userData, tweetData);
            Assert.AreEqual<int>(3, data.Count, "Expecting a Dictionary<User,List<Tweet>> instance with 3 items.");
        }

        [TestMethod()]
        public void GenerateTweetFeedTest_ValidInput()
        {
            var fileLoader = new FileLoader();
            var userData = fileLoader.LoadFile<User>(@"C:\User.txt");
            var tweetData = fileLoader.LoadFile<Tweet>(@"C:\Tweet.txt");
            var data = TweetFeedGenerator.GenerateTweetFeed(userData, tweetData);
            Assert.AreEqual<int>(3, data.Count, "Expecting a Dictionary<User,List<Tweet>> instance with 3 items.");
            Assert.AreEqual<int>(3, data[userData.FirstOrDefault((o => o.UserName == "Ward"))].Count, "Expecting a List<Tweet> instance with 3 items.");
        }
    }
}