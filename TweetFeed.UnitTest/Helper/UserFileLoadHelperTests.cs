﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TweetFeed.Data.Helper;
using System.Linq;
using TweetFeed.Data.Entity;

namespace TweetFeed.UnitTest.Helper
{
    [TestClass()]
    public class UserFileLoadHelperTests
    {
        [TestMethod()]
        public void LoadFileTest_NoInput()
        {
            var fileLoader = new FileLoader();
            var result = fileLoader.LoadFile<User>(string.Empty);
            Assert.IsFalse(result.Any(), "Expecting an empty List<User> instance.");
        }

        [TestMethod()]
        public void LoadFileTest_InvalidInput()
        {
            var fileLoader = new FileLoader();
            var result = fileLoader.LoadFile<User>(@"Q:\User.txt");
            Assert.IsFalse(result.Any(), "Expecting an empty List<User> instance.");
        }

        [TestMethod()]
        public void LoadFileTest_ValidInput()
        {
            var fileLoader = new FileLoader();
            var result = fileLoader.LoadFile<User>(@"C:\User.txt");
            Assert.AreEqual(3, result.Count, "Expecting a List<User> instance with 3 items.");
        }
    }
}