﻿namespace TweetFeed.Core.Helper
{
    /// <summary>
    ///     File where we maintain all the const param's in the system.
    /// </summary>
    public static class Constants
    {
        #region Public Constant Fields

        public const string UserDelimiter = "follows";
        public const char AssociationDelimiter = ',';
        public const string TweetDelimiter = ">";
        public const int MaxLength = 140;
        public const string InputFileExtension = ".txt";

        #endregion
    }
}